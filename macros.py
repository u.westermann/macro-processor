import csv
import subprocess
#import sys
from tabulate import tabulate
import pandas


opentag = "{{"
closetag = "}}"


def say_hello() -> str:
    """Return the string 'hello'."""
    return "hello"


def include(path: str) -> str:
    """Read file, given by path argument, return content of file."""
    with open(path) as file:
        return file.read()


def execute(command_line: str) -> str:
    """Execute external command, return stdout."""
    process_info = subprocess.run(command_line, shell=True, capture_output=True)
    return process_info.stdout.decode("ASCII")


def dict2csv(data: dict, csvfile: str) -> str:
    """Python dict to CSV file and formatted text."""
    with open(csvfile, "a") as file:
        csv.DictWriter(file, fieldnames=data.keys()).writerow(data)

    result = ""

    for key, value in data.items():
        result += f"{str(key)}: {str(value)}\n"

    return result


def csv2md(csvfile: str) -> str:
    """CSV to pandoc markdown."""
    csv_data = pandas.read_csv(csvfile, index_col=0, na_values=[], keep_default_na=False, encoding='iso8859_15')
    return tabulate(csv_data, headers="keys", tablefmt="grid")
