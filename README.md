# mp

Macro processor that uses Python as macro language.

Tested with Python 3.7, older Python 3 versions may also work.

For help, type `mp -h`.

Arbitrary Python code can be embedded in text files, example:

    This is text. Python code is embedded between double curly braces:
    42 * 42 is {{str(42 * 42) # this is Python code}}
    Say {{say_hello()}} world.

All embedded code must evaluate to string.

Python constructs like functions or variables can be defined in an external file. It's default name is `macros.py`.

To do: More pre-defined functions in macros.py.
