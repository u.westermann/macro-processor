#!/usr/bin/env python3

import argparse
import importlib
import re
import sys


def parse_arguments() -> dict:
    parser = argparse.ArgumentParser(description="Macro preprocessor that uses Python as macro language.\nRead FILE, evaluate Python code embedded between OPENTAG and CLOSETAG and substitute it with its result. All embedded code must evaluate to string. Additional Python code can be defined in MACROFILE.")

    parser.add_argument("-m", "--macrofile", default="macros",
                        help="Python file containing  definitions, omit .py extension (default: macros)")
    parser.add_argument("-o", "--opentag",
                        help="Character(s) used as start tag for embedded python code (default: opentag from macrofile, '{{' if not defined)")
    parser.add_argument("-c", "--closetag",
                        help="Character(s) used as close tag for embedded python code (default: closetag from macrofile, '}}' if not defined)")
    parser.add_argument("-e", "--environment", default="{}",
                        help="Character(s) used as close tag for embedded python code (default: closetag from macrofile, '}}' if not defined)")
    parser.add_argument("file", metavar="FILE", help="Input file.")

    args = parser.parse_args()

    return vars(args)


def substitute(input_text: str, tag_open: str, tag_close: str, eval_globals: dict) -> str:
    tag_open_esc = re.escape(tag_open)
    tag_close_esc = re.escape(tag_close)
    output_text = input_text

    for macro in re.finditer(f"{tag_open_esc}.*?{tag_close_esc}+", input_text, re.DOTALL):
        try:
            code = macro.group()[len(tag_open):-len(tag_close)]
            result = eval(code, eval_globals)
            if not isinstance(result, str):
                raise Exception("code does not evaluate to string")
            output_text = output_text.replace(macro.group(), result, 1)
        except Exception as exc:
            sys.stderr.write(f"error at pos {macro.start()}: {exc}: {macro.group()}\n")

    return output_text


if __name__ == "__main__":
    args = parse_arguments()

    input_filename = args['file']

    with open(input_filename) as input_file:
        input_text = input_file.read()

        # import python macro module
        macro_module = importlib.import_module(args['macrofile'])

        # update macro file globals with environment given on command line
        macro_module_globals = vars(macro_module)
        extra_environment = args['environment']
        environment_globals = eval(extra_environment, {'__builtins__': None})
        macro_module_globals.update(environment_globals)

        # open/close tags from command line have precedence over tags defined in macro module
        if args['opentag']:
            opentag = args['opentag']
        else:
            opentag = "{{" if macro_module.opentag is None else macro_module.opentag

        if args['closetag']:
            closetag = args['closetag']
        else:
            closetag = "}}" if macro_module.closetag is None else macro_module.closetag

        # run initialize function from macro module
        if hasattr(macro_module, 'initialize'):
            if not macro_module.initialize(input_filename):
                raise Exception("initialize failed")

        # run actual macro substitution
        print(substitute(input_text, opentag, closetag, macro_module_globals), end="")

        # run terminate function from macro module
        if hasattr(macro_module, 'terminate'):
            if not macro_module.terminate(input_filename):
                raise Exception("terminate failed")
